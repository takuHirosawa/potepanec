module ApplicationHelper
  def show_page_title(page_title)
    page_title.blank? ? "Potepanec" : page_title + " | Potepanec"
  end
end
