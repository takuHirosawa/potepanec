require 'rails_helper'

RSpec.feature "ProductsShow", type: :feature do
  let(:product) { create(:product) }

  describe 'ページレイアウト' do
    scenario 'ページに商品の情報が不足なく記載されていること' do
      visit potepan_product_path(product.id)
      expect(page.title).to eq("#{product.name} | Potepanec")
      within ".singleProduct" do
        expect(page.body).to include product.name
        expect(page.body).to include product.description
        expect(page.body).to include product.display_price.format
      end
    end
  end
end
