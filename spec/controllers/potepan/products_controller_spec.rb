require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    let(:product) { create(:product) }
    before do
      get :show, params: { id: product.id }
    end

    it "正常に200レスポンスを返すこと" do
      expect(response).to have_http_status "200"
    end

    it "showテンプレートが表示されること" do
      expect(response).to render_template(:show)
    end

    it "@productはSpree::Productのインスタンスであること" do
      expect(assigns(:product)).to eq(product)
    end
  end
end
